/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

var globals = require("./globals.js"),
    express = require("express"),
    bodyParser = require("body-parser"),
    jwt = require("jwt-simple"),
    validator = require("express-validator"),
    app = express(),
    log = __helper("log"),
    validate = __helper("validate"),
    router = express.Router(),
    cfg = __root("config");

// get the data from a POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(validator({customValidators:validate, errorFormatter: validate.errorFormatter}));


// pre-routing
app.all("*", function(req, res, next)  {

    // allow for access from any domain
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Credentials', false);
    res.header('Access-Control-Max-Age', '86400');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    res.header("Access-Control-Allow-Headers", "Authorization, Content-Type");

    // logging request and headers
    log.request(req);
    log.headers(req);
    next();
});

app.options("*", function(req, res) {
    // allow for access from any domain
    return res.status(200).end();
});

// authentication
app.use(__middleware("access"));


// token resources
app.use("/tokens", __route("tokens/create"));


// account resources
app.use("/users", __route("users/create"));
app.use("/users", __route("users/read"));
app.use("/users", __route("users/update"));


// client resources
app.use("/clients", __route("clients/create"));
app.use("/clients", __route("clients/read"));
app.use("/clients", __route("clients/update"));


// order resources
app.use("/orders", __route("orders/create"));
app.use("/orders", __route("orders/read"));
app.use("/orders", __route("orders/update"));
app.use("/orders", __route("orders/delete"));

// configurationSettings resources
app.use("/configurationSettings", __route("configurationSettings/read"));

// products resources
app.use("/products", __route("products/create"));
app.use("/products", __route("products/read"));
app.use("/products", __route("products/update"));
app.use("/products", __route("products/delete"));


// contactperson resources
app.use("/contactpersons", __route("contactpersons/create"));
app.use("/contactpersons", __route("contactpersons/read"));
app.use("/contactpersons", __route("contactpersons/update"));
app.use("/contactpersons", __route("contactpersons/delete"));


// btw resources
app.use("/btw", __route("btw/create"));
app.use("/btw", __route("btw/read"));
app.use("/btw", __route("btw/update"));
app.use("/btw", __route("btw/delete"));

// ticket resources
app.use("/tickets", __route("tickets/read"));

// invoice resources
app.use("/invoices", __route("invoices/create"));



app.use("/", router);

app.listen(cfg.port);
log.louie("IntoInternet API started listening on port " + cfg.port + " ");