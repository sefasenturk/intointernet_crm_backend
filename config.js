/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

var fs = require("fs"), cfg;

cfg = module.exports = {
    // baseURI
    baseURI: "http://intointernet.hanproject.nl",

    // web server
    port: 8080,

    // message in console debug, error, warning, info
    debug: {
        show: ["debug", "error", "headers", "warning", "info", "request", "success"]
    },

    // mysql database server
    mysql: {
        host: "149.210.218.238",
        username: "hanproject_ii",
        password: "h4nproject!",
        port: 3306,
        db: "hanproject_ii"
    },

    // account settings
    account: {
        // verification period in hours
        verificationPeriod: 48
    },

    // password security
    password: {
        iterations: 1000,
        length: 521
    },

    // token security
    token: {
        issuer: "http://intointernet.hanproject.nl",
        // expiry time in seconds after time of issue
        expires: 60*60*24*2,
        algorithm: "HS256",
        cert: fs.readFileSync("cert/test.key", "utf8"),
        key: ""

    }
};