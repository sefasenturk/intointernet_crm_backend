/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

global.__helper = function(helper) {
    return require(__dirname + "/app/helpers/" + helper + ".js");
};

global.__middleware = function(middleware) {
    return require(__dirname + "/app/middleware/" + middleware + ".js");
};

global.__route = function(route) {
    return require(__dirname + "/app/routes/" + route + ".js");
};

global.__root = function(file) {
    return require(__dirname + "/" + file + ".js");
};

module.exports = global;