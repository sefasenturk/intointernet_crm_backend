/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

var jwt = require("jwt-simple"),
    cfg = __root("config"),
    log = __helper("log"),
    validate = __helper("validate");

module.exports = function(req, res, next) {

    // skip token validation when creating new account or token
    console.log('hi');
    console.log(req.path);

    if ((req.path === "/users" && req.method === "POST") || (
        req.path.substr(0,10) === "/users/" && req.method === "PUT" && req.body.verification) ||
        req.path === "/tokens") {
        return next();
    }

    log.info("/access.js: Verify access token");
    try {
        req.token = jwt.decode(req.get("authorization").replace("Bearer ", ""), cfg.token.cert);
        if (req.token.iss !== cfg.token.issuer || req.token.aud !== "http://intointernet.hanproject.nl") {
            throw new Error();
        }
    } catch (err) {
        log.info("Token invalid");
        log.warning("401 Unauthorized");
        return res.status(401).end();
    }
    log.info("/access.js: Token valid");
    return next();
};