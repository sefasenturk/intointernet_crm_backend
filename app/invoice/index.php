<?php

if (function_exists('locale_set_default')) {
    locale_set_default('nl_NL');
}

setlocale(LC_ALL, "nl_NL");
date_default_timezone_set('Europe/Amsterdam');

set_time_limit(0);
ob_start();

define('DS', '/');
define('RL', dirname(__FILE__) . DS);
set_include_path(RL);

error_reporting(E_ALL);

require_once(RL . 'classes/libs/fpdf/fpdf.php');
require_once(RL . 'classes/Invoice.php');
require_once(RL . 'classes/Database.php');


if (isSet($_GET['handle'])) {
    if ($_GET['handle'] == 'invoice') {
//        $invoice = new Invoice(array('debug' => true, 'orderId' => $_GET['orderId']));
        $invoice = new Invoice(array('orderId' => $_GET['orderId']));

        echo json_encode(array('url' => $invoice->getLocation()));
    }
}

?>