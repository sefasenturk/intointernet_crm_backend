<?php

class Database {

    private $_connection;
    private static $_instance;

    public static function getInstance() {

        if(!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    private function __construct() {
        $this->_connection = new mysqli('localhost', 'hanproject_ii', 'h4nproject!', 'hanproject_ii');
        $this->_connection->set_charset("utf8");

        if(mysqli_connect_error()) {
            trigger_error("Failed to connect to MySQLi: " . mysqli_connect_error(), E_USER_ERROR);
        }
    }

    private function __clone() { }

    public function getConnection() {
        return $this->_connection;
    }

    public function __destruct() {
        $this->_connection->close();
    }
}

?>