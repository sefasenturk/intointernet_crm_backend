<?php

/**
 * Created by PhpStorm.
 * User: Sefa
 * Date: 21/12/2016
 * Time: 10:42
 */
class Invoice {

    private $pdf;
    private $pdfLocation;
    private $data;
    private $db;

    private $client;
    private $products;

    public function __construct($data) {
        $this->data = $data;
        $this->products = array();

        $this->db = Database::getInstance();
        $this->db = $this->db->getConnection();

        $this->getOrder($this->data['orderId']);

        $this->pdf = new FPDF('Portrait', 'mm', 'A4');
        $this->pdf->SetFont('Arial', '', 12);

        $this->pdf->AddPage();

        $this->Header();
        $this->Content();
        $this->Footer();

        $this->Save();
    }

    private function getOrder($orderId) {
        $client = $this->db->query("SELECT *
                                      FROM `order` O 
                                INNER JOIN `client` C 
                                        ON O.`clientnumber` = C.`clientnumber` 
                                INNER JOIN address A
                                        ON A.addressnumber = C.addressnumber
                                     WHERE O.`ordernumber` = '$orderId'") or die ($this->db->error);

        $this->client = $client->fetch_assoc();

        $products = $this->db->query("SELECT P.productnumber, P.`name` AS productname, P.`price`, P.`description`, PO.`discount` AS discount 
                                        FROM `product` P 
                                  INNER JOIN `productorder` PO 
                                          ON P.`productnumber` = PO.`productnumber` 
                                       WHERE PO.`ordernumber` = '$orderId'") or die ($this->db->error);

        while ($p = $products->fetch_assoc()) {
            $this->products[] = $p;
        }
    }

    private function Header() {
        $this->pdf->Image(RL . 'images/logo.png', 17.5, 12, 59);

        $this->pdf->setY($this->pdf->getY() + 24);
        $this->pdf->setX($this->pdf->getX() + 17);

        $this->pdf->SetTextColor(53, 117, 183);
        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->Write(5, 'WWW.INTOINTERNET.NL', 'http://www.intointernet.nl');

        $this->pdf->setY(20);
        $this->pdf->setX(90);
        $this->pdf->Write(5, 'INTOINTERNET BV');

        $this->pdf->SetTextColor(122, 122, 122);

        $this->pdf->Ln();
        $this->pdf->setX(90);
        $this->pdf->Write(5, 'KELVINSTRAAT 36');

        $currentY = $this->pdf->getY();

        $this->pdf->Ln();
        $this->pdf->setX(90);
        $this->pdf->Write(5, '6601HE WIJCHEN');


        $this->pdf->setY($currentY);
        $this->pdf->setX(136);
        $this->pdf->Write(5, '088 - 999 5 666');

        $this->pdf->Ln();
        $this->pdf->setX(136);
        $this->pdf->Write(5, 'INFO@INTOINTERNET.NL', 'mailto:info@intointernet.nl');
        $this->pdf->Ln();

        $this->pdf->setY(62);
        $this->pdf->setX(20);
        $this->pdf->Write(5, $this->client['name']);

//        $this->pdf->Ln();
//        $this->pdf->setX(20);
//        $this->pdf->Write(5, 'T.a.v. ' . $this->client['']);

        $this->pdf->Ln();
        $this->pdf->setX(20);
        $this->pdf->Write(5, $this->client['street'] . ' ' . $this->client['housenumber'] . ' ' . $this->client['extension']);

        $this->pdf->Ln();
        $this->pdf->setX(20);
        $this->pdf->Write(5, $this->client['zip'] . ' ' . $this->client['city']);
        $this->pdf->Ln(4);
    }

    private function Content() {
        $this->pdf->SetFont('Arial', 'B', 15);
        $this->pdf->SetTextColor(53, 117, 183);
        $this->pdf->SetXY(20, 100);
        $this->pdf->Write(5, 'FACTUUR');
        $this->pdf->Ln();
        $this->pdf->Ln();

        $this->pdf->SetTextColor(0, 0, 0);
        $this->pdf->SetFont('Arial', '', 8);

        $this->pdf->SetX(20);
        $this->pdf->Cell(45, 5, 'DATUM');
        $this->pdf->Cell(110, 5, date('d-m-Y'));
        $this->pdf->Ln();

        $this->pdf->SetX(20);
        $this->pdf->Cell(45, 5, 'FACTUURNUMMER');
        $this->pdf->Cell(110, 5, rand(1000, 4000));
        $this->pdf->Ln();

        $this->pdf->SetX(20);
        $this->pdf->Cell(45, 5, 'OMSCHRIJVING');
        $this->pdf->Cell(110, 5, 'Abonnement periode ' . date('01-m-Y') . ' tot ' . date('t-m-Y'));
        $this->pdf->Ln();

        $this->pdf->SetX(20);
        $this->pdf->Cell(45, 5, 'UW REFERENTIE');
        $this->pdf->Cell(110, 5, 'nvt');
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', 9);
        $this->pdf->SetXY(20, $this->pdf->GetY() + 10);
        $this->pdf->SetDrawColor(88, 88, 88);
        $this->pdf->Cell(20, 10, 'AANTAL', 'B', 'B');
        $this->pdf->Cell(105, 10, 'OMSCHRIJVING', 'B');
        $this->pdf->Cell(22, 10, 'BEDRAG', 'B', 0, 'R');
        $this->pdf->Cell(22, 10, 'TOTAAL', 'B', 0, 'R');
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', '', 9);

        $totalPrice = 0;
        $discountPercentage = 0;

        foreach ($this->products as $line) {

            $this->pdf->SetX(20);
            $this->pdf->Cell(20, 10, $this->number(1), 0);
            $this->pdf->Cell(105, 10, $line['productname'], 0);
            $this->pdf->Cell(22, 10, $this->price($line['price']), 0, 0, 'R');
            $totalPrice += 1 * $line['price'];
            $this->pdf->Cell(22, 10, $this->price((1 * $line['price'])), 0, 0, 'R');
            $this->pdf->Ln();

            if ($line['discount']) {
                $discountPercentage += $line['discount'];
            }
        }

        if ($discountPercentage > 0) {
            $this->pdf->SetX(20);
            $this->pdf->Cell(20, 10, '', 0);
            $this->pdf->Cell(105, 10, '', 0);
            $this->pdf->Cell(22, 10, 'Korting ' .  $discountPercentage . '%', 'T', 0, 'R');

            $discount = $totalPrice / 100 * $discountPercentage;

            $totalPrice -= $discount;
            $this->pdf->Cell(22, 10, $this->price($discount), 'BT', 0, 'R');
            $this->pdf->Ln();
        }

        $this->pdf->SetX(20);
        $this->pdf->Cell(20, 10, '', 0, 'B');
        $this->pdf->Cell(105, 10, '', 0);
        $this->pdf->Cell(22, 10, 'Subtotaal', '', 0, 'R');
        $this->pdf->Cell(22, 10, $this->price($totalPrice), 'B', 0, 'R');
        $this->pdf->Ln();

        $this->pdf->SetX(20);
        $this->pdf->Cell(20, 10, '', 0, 'B');
        $this->pdf->Cell(105, 10, '', 0);
        $this->pdf->Cell(22, 10, '21% BTW', 0, 0, 'R');

        $vat = $totalPrice / 100 * 21;
        $this->pdf->Cell(22, 10, $this->price($vat), 'BT', 0, 'R');
        $this->pdf->Ln();

        $this->pdf->SetFont('Arial', 'B', 9);

        $this->pdf->SetX(20);
        $this->pdf->Cell(20, 10, '', 0, 'B');
        $this->pdf->Cell(105, 10, '', 0);
        $this->pdf->Cell(22, 10, 'Totaal', 0, 0, 'R');

        $totalInclVat = $totalPrice + $vat;
        $this->pdf->Cell(22, 10, $this->price($totalInclVat), 'BT', 0, 'R');
        $this->pdf->Ln();
    }

    private function price($pr) {
        return chr(128) . ' ' . number_format($pr, 2, ',', '.');
    }

    private function number($nr) {
        return number_format($nr, 2, ',', '.');
    }

    private function Footer() {
        $this->pdf->Ln(13);
        $this->pdf->SetX(20);
        $this->pdf->SetFont('Arial', '', 9);

        $this->pdf->MultiCell(100, 5, "Dit bedrag zal tussen 5 tot 10 werkdagen automatisch worden afgeschreven van rekeningnummer NL51RABO0174984421, met machtingskenmerk I5504.\n\nOns incassant ID is NL61ZZZ645170470000.");

//        $this->pdf->SetY(-15);
//        $this->pdf->Write(5, 'KVK');
    }

    private function Save() {
        $this->pdfLocation = 'temp/invoice.pdf';
        if (isSet($this->data['debug'])) {
            $this->pdf->Output($this->pdfLocation, 'I');
        } else {
            $this->pdf->Output($this->pdfLocation, 'F');
        }
    }

    public function getLocation($absolutePath = false) {
        if ($absolutePath) {
            return $this->pdfLocation;
        }

        return 'temp/invoice.pdf';
    }
}

?>