/**
 * Created by Sefa on 06/12/2016.
 */
// * To add a console log messages in your code:
//     * - log.debug("This prints a blue text in the console");
// * - log.error("This prints a red text in the console");
// * - log.info("This prints a green text in the console");
// * - log.warning("This prints a yellow text in the console");
"use strict";

var colors = require("colors"),
    log;

colors.setTheme({
    silly: 'rainbow',
    input: 'grey',
    verbose: 'cyan',
    prompt: 'grey',
    info: 'green',
    data: 'grey',
    help: 'cyan',
    warn: 'yellow',
    debug: 'blue',
    error: 'red'
});


log = module.exports = {

    // method to output debug message
    debug: function (msg) {
        console.log(colors.debug(msg));
    },

    // method to output error message
    error: function (msg) {
        console.log(colors.error(msg));
    },

    // method to output server message
    louie: function (msg) {
        console.log(colors.verbose(msg.bold));
        var line = "";
        for(var i = 1; i < msg.length; i++) {line += "-"}
        console.log(colors.verbose(line));
    },

    // method to output http request headers
    headers: function(req) {
        for(var header in req.headers) {
            console.log((header.bold + ": " + req.get(header).italic).magenta);
        }
    },

    // method to output info message
    info: function (msg) {
        console.log(colors.data(msg));
    },

    // method to output http requests
    request: function(req) {
        console.log(((req.method + " " + req.path).bold).magenta);
    },

    // method to output success message
    success: function (msg) {
        console.log(colors.info(msg.bold));
    },

    // method to output warning message
    warning: function (msg) {
        console.log(colors.warn(msg.bold));
    }
};