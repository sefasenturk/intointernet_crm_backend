/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

var log = __helper("log"),
    validate;

validate = {
    isBIC: function(code) {
        var pattern =  /^([a-zA-Z]){4}([a-zA-Z]){2}([0-9a-zA-Z]){2}([0-9a-zA-Z]{3})?$/
        return validate.string(code,undefined, undefined, pattern);
    },

    /**
     * Email validation method
     * @param {*} address - The value to be tested
     * @returns {boolean} - True if the value is a valid email addres, false otherwise
     * @see https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address
     */
    isEmail: function (address) {
        // check whether the value is defined
        if (!validate.defined(address)) {
            return false;
        }
        // set email pattern
        var pattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

        // Test value against email pattern and return outcome
        return pattern.test(address);
    },

    /**
     * Validation method for IBAN number
     * @param {string} nr - value to be tested
     * @returns {boolean} - true if the nr is a valid IBAN, false otherwise
     */
    isIBAN: function(nr) {
        var pattern = /[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}/
        return validate.string(nr, undefined, undefined, pattern);
    },

    isURL: function (url) {
        var pattern = /^(https?:\/\/)?[0-9a-zA-Z]+\.[-_0-9a-zA-Z]+\.[0-9a-zA-Z]+$/
        if (!validate.string(url.toLowerCase(), undefined, undefined, pattern)) {
            return false;
        }
        return true;
    },

    isVAT: function (nr) {
        var pattern = /^((AT)?U[0-9]{8}|(BE)?0[0-9]{9}|(BG)?[0-9]{9,10}|(CY)?[0-9]{8}L|(CZ)?[0-9]{8,10}|(DE)?[0-9]{9}|(DK)?[0-9]{8}|(EE)?[0-9]{9}|(EL|GR)?[0-9]{9}|(ES)?[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}|(FR)?[0-9A-Z]{2}[0-9]{9}|(GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})|(HU)?[0-9]{8}|(IE)?[0-9]S[0-9]{5}L|(IT)?[0-9]{11}|(LT)?([0-9]{9}|[0-9]{12})|(LU)?[0-9]{8}|(LV)?[0-9]{11}|(MT)?[0-9]{8}|(NL)?[0-9]{9}B[0-9]{2}|(PL)?[0-9]{10}|(PT)?[0-9]{9}|(RO)?[0-9]{2,10}|(SE)?[0-9]{12}|(SI)?[0-9]{8}|(SK)?[0-9]{10})$/
        if (!validate.string(nr, 4, undefined, pattern)) {
            return false;
        }
        return true;
    },

    // check wether or not a value is defined
    /**
     * Method to verify whether or not a given variable is defined.
     * @param value - value to be tested
     * @returns {boolean} - true if a value has been defined, false otherwise
     */
    defined: function (value) {
        return typeof value !== "undefined";
    },
    /**
     * Method to validate an hexadecimal number string of 32 characters
     * @param value - value to be tested
     * @returns {boolean} - true if the value is a hexadecimal number string of 32 characters, false otherwise
     */
    hex32: function (value) {
        return validate.string(value, 32, 32, /^[A-Fa-f0-9]{32}$/);
    },

    /**
     * Method to check whether a string is not exceeding a given maximum amount of characters
     * @param txt - string to be tested
     * @param max - maximum amount of characters
     * @returns {boolean} - true if the string does not exceed the maximum length, false otherwise
     */
    maxLength: function (txt, max) {
        return !(txt.length > max);
    },

    /**
     * Method to check whether a string is not shorter than a given minimum amount of characters
     * @param txt - string to be tested
     * @param max - minimum amount of characters
     * @returns {boolean} - true if the string has the minimum length, false otherwise
     */
    minLength: function (txt, min) {
        return !(txt.length < min);
    },

    /**
     * Number validation method
     * @param {*} value - value to be tested
     * @param {number} min - The number cannot be less than this
     * @param {number} max: The number cannot be greater than this
     * @returns {boolean}
     */
    number: function (value, min, max) {
        // check whether value is defined
        if (!validate.defined(value)) {
            return false;
        }
        // check whether value is a number
        if (typeof value !== "number") {
            return false;
        }
        // check whether the number is not less than the minimum required
        if (validate.defined(min)) {
            if (value < min) {
                return false;
            }
        }
        // check whether the number is not greater than the maximum required
        if (validate.defined(max)) {
            if (value > max) {
                return false;
            }
        }
        return true;
    },


    /**
     * String validation method
     * @param {number} min - minimum length required
     * @param {number} max -maximum length allowed
     * @param {RegExp} rx - pattern to be matched
     * @returns {boolean} - true if txt is a string, does not exceed the boundaries of min, max and matched the rx
     */
    string: function (txt, min, max, rx) {
        if (!validate.defined(txt)) {
            return false;
        }
        // check if txt is a string
        if (!(typeof txt === "string")) {
            return false;
        }
        // check if txt has the minimum required length
        if (validate.defined(min)) {
            if (!validate.minLength(txt, min)) {
                return false;
            }
        }
        // check if txt does not exceed the maximum length allowed
        if (validate.defined(max)) {
            if (!validate.maxLength(txt, max)) {
                return false;
            }
        }
        // check if txt matches the required pattern
        if (validate.defined(rx)) {
            if (!rx.test(txt)) {
                return false;
            }
        }
        return true;
    },

    // validation method for password
    isPassword: function (_v) {
        return validate.hex32(_v);
    }
};

module.exports = validate;