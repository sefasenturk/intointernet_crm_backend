/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

var mysql = require('mysql'),
    log = __helper("log"),
    cfg = __root("config");

var connection = mysql.createConnection({
    host     : cfg.mysql.host,
    user     : cfg.mysql.username,
    password : cfg.mysql.password,
    database : cfg.mysql.db
});

connection.connect(function(err) {
    if (err) {
        log.error(err);
    }

    log.info("MySQL module initialized connection.");
});

module.exports = connection;