/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

var crypto = require("crypto"),
dlog = __helper("log"),
lib;

lib = {
    pwd: function(password, salt, iterations) {
        var password = crypto.pbkdf2Sync(password.toString(), salt, iterations, 512, "sha256").toString("base64");
        return password;
    },
    random: function(size, base) {
        return crypto.randomBytes(size).toString(base);
    }

};

module.exports = lib;
