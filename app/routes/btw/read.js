/**
 * Created by Robert on 12/7/16.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    db =__helper("database");

router.route("/").get(function (req, res) {
    log.info("/products/read.js: Get all btw");

    db.query("SELECT * FROM btw", function(err, rows) {
        if (err) {
            return res.status(309).json(err);
        }

        return res.status(200).json(rows);
    });
});

router.route("/:id").get(function(req,res) {
    log.info("/products/read.js: Get one btw by id");

    db.query("SELECT * FROM btw WHERE btw = ?", [req.params.btw],function(err, rows) {
        if (err) {
            throw err;
        }
        if (rows.length == 0){
            log.warning("btw not found: 401 Unauthorized");
            return res.status(401).json({ succes: false});
        }
        if(rows.length > 1){
            log.warning("Multiple products found! This should not be happening");
            return res.status(409).json({succes: false});
        }
        var btw = rows[0];
        log.info(btw);
        return res.status(201).json(v);
    });
});

module.exports = router;