/**
 * Created by Robert on 12/7/16.
 */

"use strict";

var express = require("express"),
    router = express.Router(),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    db =__helper("database"),
    log = __helper("log"),
    file = "/btw/create.js: ";

router.route("/").post(function (req, res) {

    log.info(file + ": Create btw");

    var btw = req.body.btw;
    db.query("INSERT INTO btw VALUES (?)",
        [
            btw
        ],
        function(err, result) {
            if (err) {
                log.error(file + " MySQL error");
                log.error(err);

                return res.status(409).json({ success: false, error: err });
            }

            return res.status(201).json({ success: true });
        });
});

module.exports = router;