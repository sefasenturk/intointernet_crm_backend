/**
 * Created by Robert on 12/7/16.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    db = __helper("database"),
    validate = __helper("validate"),
    file = "products/update.js",
    errors, id, updates, data, company;

router.route("/:id").put(function (req, res) {
    log.info("/products/update.js: Update product by id");

    var btwold = req.body.btwold;
    var btwnew = req.body.btwnew;

    db.query("UPDATE btw SET btw = ? WHERE btw = ?",
        [
            btwnew,
            btwold
        ],
        function( err, result){
            if (err){
                log.error(file + " MySQL error");
                log.error(err);

                return res.status(409).json({ success: false, error: err });
            }

            log.info("product updated");
            return res.status(201).json({ success: true });
        });

});

module.exports = router;