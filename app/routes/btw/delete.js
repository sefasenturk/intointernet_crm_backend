/**
 * Created by Robert on 12/7/16.
 */

"use strict";

var express = require("express"),
    router = express.Router(),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    db = __helper("database"),
    log = __helper("log"),
    file = "/btw/delete.js:";

router.route("/:id").delete(function (req, res) {
    log.info(file + " delete btw");

    db.query(" DELETE FROM btw WHERE btw = ?", req.param('btw'),function(err, rows) {
        if (err) {
            console.log(err);
            return res.status(304).json(err);
        }

        return res.status(200).json(rows);
    });

});

module.exports = router;