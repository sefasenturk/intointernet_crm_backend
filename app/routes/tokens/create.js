/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

var express = require("express"),
    jwt = require("jwt-simple"),
    rfc4122 = require("rfc4122"),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    log = __helper("log"),
    router = express.Router(),
    db = __helper("database"),
    errors, payload, token, file = "/tokens/create.js: ";

router.route("/").post(function (req, res) {

    log.info("/tokens/create.js: Create token");

    // req.checkBody("email").isEmail();
    // req.checkBody("password").isPassword();

    errors = req.validationErrors();
    if (errors) {

        log.warning("/tokens/create.js: Payload validation did not succeed: respond 400 Bad Request");
        return res.status(400).json(errors);
    }

    log.info(file + "Payload validation succeeded: find the user and his role by username: " + req.body.username);
    db.query("SELECT * FROM user U INNER JOIN user_userrole UU ON U.username = UU.username WHERE U.username = ?", [req.body.username], function(err, rows) {
        if (err) {
            log.error(file + "MySQL error while selecting user from email");
            log.error(err);

            return res.status(409).json(err);
        }

        if (rows.length == 0) {
            log.warning(file + "User is not found: 401 Unauthorized");
            return res.status(401).json({ success: false });
        }

        if (rows.length > 1) {
            console.log(rows);
            log.error(file + "There are multiple users found, this means that there are multiple users with the same username. Cant be happening!");
            return res.status(409).json({ success: false });
        }

        var user = rows[0];

        log.info(file + "User found, checking credentials");

        if (user.password !== crypto.pwd(req.body.password, user.salt, parseInt(user.iteration))) {
            log.warning("/tokens/create.js: User is not found: 401 Unauthorized");
            return res.status(401).json({ success: false });
        }

        log.info(file + "Account found: Create, sign and return token");

        // Create JWT payload
        payload = {
            iss: cfg.token.issuer,
            sub: user.username,
            role: user.userrole,
            nbf: Math.floor(new Date().getTime() / 1000),
            iat: Math.floor(new Date().getTime() / 1000),
            exp: Math.floor(new Date().getTime() / 1000) + (cfg.token.expires),
            jti: new rfc4122().v4(),
            aud: "http://intointernet.hanproject.nl"
        };

        token = jwt.encode(payload, cfg.token.cert);

        log.success(file + "201 Created");
        res.status(201).json({
            success: true,
            token: token
        });
    });
});

module.exports = router;