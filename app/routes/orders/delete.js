"use strict";

var express = require("express"),
    router = express.Router(),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    db = __helper("database"),
    log = __helper("log");


router.route("/:id").delete(function (req, res) {
    log.info("/orders/delete.js: delete order");

    db.query("CALL deleteOrder(?)", [req.params.id] ,function(err) {
        if (err) {
            return res.status(409).json({success: false, error: err});
        }
        else {
            return res.status(201).json({success: true});
        }
    });

});

module.exports = router;