"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    db =__helper("database"),
    file = "/order/read.js: ",
    async = require('async');

router.route("/").get(function (req, res) {
    log.info(file + "Get all orders");

    db.query("SELECT O.ordernumber, C.name FROM `order` O INNER JOIN client C ON O.clientnumber=C.clientnumber", function(err, rows) {
        if (err) {
            throw err;
        }

        return res.status(200).json(rows);
    });
});

router.route("/:id").get(function(req,res) {
    log.info(file + "Get one order by id");

    db.query("SELECT C.`name` AS clientname, C.`coc`, C.`iban`, O.`ordernumber` AS ordernumber , O.`billingdate` FROM `order` O INNER JOIN `client` C ON O.`clientnumber` = C.`clientnumber` WHERE O.`ordernumber` = ?", [req.params.id], function(err, client) {
        if (err) {
            return res.status(409).json({ success: false, error: err });
        }

        client = client[0];

        db.query("SELECT `status` AS mayEdit FROM `invoice` WHERE ordernumber = ?", [req.params.id], function(err, invoice) {
            if (err) {
                throw err;
            }

            client.invoice = invoice[0];

            db.query("SELECT `ordernumber`, `appointmentdate`, `description`, `estimation` FROM `appointment` WHERE ordernumber = ?", [req.params.id], function(err, appointments) {
                if (err) {
                    throw err;
                }

                client.appointments = appointments;


                    db.query("SELECT P.productnumber, P.`name` AS productname, P.`price`, P.`description`, PO.`discount` AS discount FROM `product` P INNER JOIN `productorder` PO ON P.`productnumber` = PO.`productnumber` WHERE PO.`ordernumber` = ?", [req.params.id], function (err, products) {
                        if (err) {
                            return res.status(409).json({success: false, error: err});
                        }

                        async.forEachOf(products, function (value, key, callback) {

                            db.query("SELECT configname, configdescription FROM configurationsettings WHERE ordernumber=? AND productnumber=?", [req.params.id, value.productnumber], function (err, settings) {
                                if (err) {
                                    callback(err);
                                }

                                products[key].configSettings = settings;
                                callback();
                            });

                        }, function (err) {
                            if (err) {
                                return res.status(409).json({success: false, error: err});
                            }

                            client.products = products;

                            log.info(file + "Order returned");
                            return res.status(200).json(client);
                    });
                });
            });
        });
    });
});

module.exports = router;