"use strict";

var express = require("express"),
    router = express.Router(),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    db =__helper("database"),
    log = __helper("log");

router.route("/").post(function (req, res) {
    log.info("/orders/create.js: Create order");
    log.info(JSON.stringify(req.body.order));
    var order = req.body.order;
    var products = req.body.products;
    db.query("INSERT INTO `order` (clientnumber, billingdate, billinginterval) VALUES (?,?,?);",
        [order.clientNumber, order.billingDate, order.billingInterval],
        function(err) {
        if (err) {
            log.info(err);
            return res.status(409).json({ success: false, error: err });
        }
        db.query("INSERT INTO appointment (ordernumber, appointmentdate, description, estimation) VALUES ((SELECT MAX(ordernumber) FROM `order`),?,?,?);",
            [order.appointment.date, order.appointment.desc, order.appointment.estimation],
            function(err) {
                if (err) {
                    return res.status(409).json({ success: false, error: err });
                }
                else {
                    var length = products.length;
                    var count = 0;
                    for (var i = 0; i < length; i++) {
                        db.query("INSERT INTO `productorder` (ordernumber,productnumber,discount) VALUES ((SELECT MAX(ordernumber) FROM `order` WHERE clientnumber=?),?,?);",
                            [order.clientNumber, products[i].productnumber, products[i].discount],
                            function (err) {
                                if (err) {
                                    return res.status(409).json({success: false, error: err});
                                }
                                if (count == length - 1) {
                                    return res.status(201).json({success: true});
                                }
                                count++;
                            });
                    }
                }
        });
    });
});

module.exports = router;