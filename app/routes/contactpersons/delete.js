/**
 * Created by Sefa on 19/12/2016.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    db =__helper("database"),
    file = "/contactpersons/delete.js: ";

router.route("/:id/:clientId").delete(function (req, res) {
    log.info(file + "Delete contactperson");

    db.query("CALL deleteContactperson(?, ?)", [req.params.id, req.params.clientId], function(err, result) {
        if (err) {
            log.error(file + "MySQL error");
            log.error(err);

            return res.status(409).json({ success: false, error: err });
        }

        return res.status(201).json({ success: true, res: result });
    });
});

module.exports = router;