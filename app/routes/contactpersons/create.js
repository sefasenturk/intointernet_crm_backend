/**
 * Created by Sefa on 15/12/2016.
 */
var express = require("express"),
    router = express.Router(),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    db =__helper("database"),
    log = __helper("log"),
    file = "/contactpersons/create.js: ";



router.route("/").post(function (req, res) {

    log.info(file + "Create contactperson");

    db.query("INSERT INTO address SET ?", req.body.address, function(err, result) {
        console.log(err, result);

        if (err) {
            log.error(file + "MySQL error");
            log.error(err);

            return res.status(409).json({ success: false, error: err });
        }

        req.body.contactperson.addressnumber = result.insertId;

        db.query("CALL proc_insert_contactperson(?, ?, ?, ?, ?, ?, ?, ?)", [req.body.contactperson.addressnumber, req.body.contactperson.title, req.body.contactperson.role, req.body.contactperson.initials, req.body.contactperson.firstname, req.body.contactperson.lastname, req.body.contactperson.companyname, req.body.clientnumber], function(err, result) {
            if (err) {
                db.query("DELETE FROM address WHERE addressnumber = ?", [req.body.contactperson.addressnumber]);

                log.error(file + "MySQL error");
                log.error(err);

                return res.status(409).json({ success: false, error: err });
            }

            return res.status(201).json({ success: true});

            // var contactpersonnumber = result.insertId;
            //
            // db.query("INSERT INTO contactperson_client SET ?", { contactpersonnumber: contactpersonnumber, clientnumber: req.body.clientnumber }, function (err, result) {
            //     if (err) {
            //         db.query("DELETE FROM contactperson WHERE contactpersonnumber = ?", [contactpersonnumber]);
            //         db.query("DELETE FROM address WHERE addressnumber = ?", [req.body.contactperson.addressnumber]);
            //
            //         log.error(file + "MySQL error");
            //         log.error(err);
            //
            //         return res.status(409).json({ success: false, error: err });
            //     }
            //
            //     return res.status(201).json({ success: true, contactpersonnumber: contactpersonnumber});
            //
            // });
        });
    });


});

module.exports = router;