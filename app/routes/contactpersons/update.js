/**
 * Created by Sefa on 15/12/2016.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    validate = __helper("validate"),
    db =__helper("database"),
    file = "/contactpersons/update.js: ";

router.route("/:id").put(function (req, res) {
    log.info(file + "Update contactperson by id");


    db.query("UPDATE address SET street = ?, housenumber = ?, extension = ?, zip = ?, city = ?, email = ?, phonenumber = ? WHERE addressnumber = ?", [req.body.address.street, req.body.address.housenumber, req.body.address.extension, req.body.address.zip, req.body.address.city, req.body.address.email, req.body.address.phonenumber, req.body.addressnumber], function(err, result) {
        if (err) {
            log.error(file + "MySQL error");
            log.error(err);

            return res.status(409).json({ success: false, error: err });
        }

        db.query("UPDATE contactperson SET title = ?, role = ?, initials = ?, firstname = ?, lastname = ?, companyname = ? WHERE contactpersonnumber = ?", [req.body.contactperson.title, req.body.contactperson.role, req.body.contactperson.initials, req.body.contactperson.firstname, req.body.contactperson.lastname, req.body.contactperson.companyname, req.params.id], function(err, result) {
            if (err) {
                log.error(file + "MySQL error");
                log.error(err);

                return res.status(409).json({ success: false, error: err });
            }

            return res.status(201).json({ success: true });
        });
    });
});

module.exports = router;