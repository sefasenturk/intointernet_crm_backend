/**
 * Created by Sefa on 15/12/2016.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    db =__helper("database"),
    file = "/contactpersons/read.js: ";

router.route("/").get(function (req, res) {
    log.info(file + "Get all contactpersons");

    db.query("SELECT * FROM contactperson C INNER JOIN address A ON C.addressnumber = A.addressnumber", function(err, rows) {
        if (err) {
            throw err;
        }

        return res.status(200).json(rows);
    });
});

module.exports = router;