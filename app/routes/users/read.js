/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log");

router.route("/").get(function (req, res) {
    log.info("/users/read.js: Get all users");
});

router.route("/:id").get(function(req,res) {
    log.info("/users/read.js: Get one user by id");
});

module.exports = router;