/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    validate = __helper("validate"),
    errors, id, updates, data, company;

router.route("/:id").put(function (req, res) {
    log.info("/users/update.js: Update user by id");
});

module.exports = router;