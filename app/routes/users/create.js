/**
 * Created by Sefa on 06/12/2016.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    log = __helper("log");

router.route("/").post(function (req, res) {

    log.info("/users/create.js: Create user");

    var salt = crypto.random(100, "hex");
    // var iterations = cfg.password.iterations;
    // var password = "asd123";
    //
    // console.log("Salt", salt);
    // console.log("Iterations", iterations);
    // console.log("Password plain", password);
    console.log("Password hashed", crypto.pwd(password, salt, iterations));


});

module.exports = router;