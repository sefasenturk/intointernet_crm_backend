"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    db =__helper("database"),
    file = "/ticket/read.js: ";

router.route("/").get(function (req, res) {
    log.info(file + "Get all tickets");

    db.query("SELECT * FROM ticket", function(err, rows) {
        if (err) {
            throw err;
        }
        return res.status(200).json(rows);
    });
});

module.exports = router;