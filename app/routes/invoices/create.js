/**
 * Created by Sefa on 23/12/2016.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    db =__helper("database"),
    log = __helper("log"),
    file = "/invoices/create.js: ",
    request = require('request');

router.route("/:orderId").get(function (req, res) {

    log.info(file + "Generate invoice");

    request.get({ url: 'http://maatwerk.focusws.nl/intointernet-invoice/?handle=invoice&orderId=' + req.params.orderId, json: true }, function (error, response, data) {
        if (!error && response.statusCode == 200) {
            return res.status(200).json({success: true, invoiceURL: data.url});
        } else {
            return res.status(409).json({success: false, error: error});
        }
    });
});

module.exports = router;