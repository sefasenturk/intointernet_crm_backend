"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    db =__helper("database"),
    file = "/configurationSettings/read.js: ";

router.route("/:oid/:pid").get(function(req,res) {
    log.info(file + "Get one configurationSettings by id's");
    db.query("SELECT configname, configdescription FROM configurationsettings WHERE ordernumber=? AND productnumber=?", [req.params.oid, req.params.pid], function(err, rows) {
        if (err) {
            return res.status(409).json({success: false, error: err});
        }
        return res.status(200).json({data:rows});
    });
});

module.exports = router;