/**
 * Created by Sefa on 08/12/2016.
 */
var express = require("express"),
    router = express.Router(),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    db =__helper("database"),
    log = __helper("log"),
    file = "/clients/create.js: ";



router.route("/").post(function (req, res) {

    log.info(file + "Create user");

    //TODO: Validate data
    var user = req.body.user;
    var address = req.body.address;
    var client = req.body.client;
    var contactperson = req.body.contactperson;
    var salt = crypto.random(100, "hex");

    db.query("CALL insertFirstContactpersonClient(" +
        // "## ADDRESS CONTACTPERSON " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        // "## CONTACTPERSON " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        // "## USER " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        // "## ADDRESS CLIENT " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        "?, " +
        // "## CLIENT " +
        "?, " +
        "?, " +
        "?, " +
        "?)",
        [
            contactperson.street,
            contactperson.housenumber,
            contactperson.extension,
            contactperson.zip,
            contactperson.city,
            contactperson.email,
            contactperson.phonenumber,
            contactperson.title,
            contactperson.role,
            contactperson.initials,
            contactperson.firstname,
            contactperson.lastname,
            contactperson.companyname,
            user.username,
            crypto.pwd(user.password, salt, cfg.password.iterations),
            salt,
            cfg.password.iterations,
            address.street,
            address.housenumber,
            address.extension,
            address.zip,
            address.city,
            address.email,
            address.phonenumber,
            client.name,
            client.iban,
            client.bic,
            client.coc
        ],
        function(err, result) {
            if (err) {
                log.error(file + "MySQL error while calling stored procedure");
                log.error(err);

                return res.status(409).json({ success: false, error: err });
            }

            log.info(file + "Client created");
            return res.status(201).json({ success: true });
        });
});

module.exports = router;