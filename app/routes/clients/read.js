/**
 * Created by Kevin on 17/01/2017.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    db =__helper("database"),
    file = "/clients/read.js: ";

router.route("/").get(function (req, res) {
    log.info(file + "Get all clients");

    db.query("SELECT * FROM client C INNER JOIN address A ON C.addressnumber = A.addressnumber", function(err, rows) {
        if (err) {
            throw err;
        }

        return res.status(200).json(rows);
    });
});

router.route("/:id").get(function(req,res) {
    log.info("/clients/read.js: Get one client by id");

    db.query("SELECT * FROM client C INNER JOIN address A ON C.addressnumber = A.addressnumber WHERE clientnumber = ?", [req.params.id], function (err, client) {
        if (err) {
            return res.status(409).json({success: false, error: err});
        }
        client = client[0];
        db.query("SELECT * FROM contactperson C INNER JOIN contactperson_client CC ON CC.contactpersonnumber = C.contactpersonnumber INNER JOIN address A ON A.addressnumber = C.addressnumber WHERE CC.clientnumber = ?", [req.params.id], function (err, rows) {
            if (err) {
                return res.status(409).json({success: false, error: err});
            }
            client.contactpersons = rows;
            db.query("SELECT ordernumber FROM `order` WHERE clientnumber = ?", [req.params.id], function (err, rows) {
                if (err) {
                    return res.status(409).json({success: false, error: err});
                }
                client.orders = rows;
                return res.status(200).json(client);
            });
        });
    });
});

module.exports = router;