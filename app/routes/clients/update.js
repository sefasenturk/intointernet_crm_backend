/**
 * Created by Sefa on 16/12/2016.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    validate = __helper("validate"),
    cfg = __root("config"),
    db = __helper("database"),
    crypto = __helper("crypto"),
    file = "/clients/update.js: ";

router.route("/:id").put(function (req, res) {
    log.info(file + "Update client by id");

    var addressNumber = req.body.address.addressnumber;
    delete req.body.address.addressnumber;
    var address = req.body.address;

    var client = req.body.client;


    db.query("UPDATE address SET street = ?, housenumber = ?, extension = ?, zip = ?, city = ?, email = ?, phonenumber = ? WHERE addressnumber = ?", [address.street, address.housenumber, address.extension, address.zip, address.city, address.email, address.phonenumber, addressNumber], function(err, result) {
        if (err) {
            log.error(file + "MySQL error while updating address");
            log.error(err);

            return res.status(409).json({ success: false, error: err });
        }

        db.query("UPDATE client SET name = ?, iban = ?, bic = ?, coc = ? WHERE clientnumber = ?", [client.name, client.iban, client.bic, client.coc, req.params.id], function(err, result) {
            if (err) {
                log.error(file + "MySQL error while updating client");
                log.error(err);

                return res.status(409).json({ success: false, error: err });
            }

            if (req.body.user.password !== undefined) {
                var salt = crypto.random(100, "hex");

                db.query("UPDATE user SET password = ?, salt = ?, iteration = ? WHERE username = ?", [crypto.pwd(req.body.user.password, salt, cfg.password.iterations), salt, cfg.password.iterations, req.body.user.username], function(err, result) {
                    if (err) {
                        log.error(file + "MySQL error while updating user");
                        log.error(err);

                        return res.status(409).json({ success: false, error: err });
                    }

                    log.info(file + "Client updated");
                    return res.status(201).json({ success: true });
                });
            } else {
                log.info(file + "Client updated");
                return res.status(201).json({ success: true });
            }
        });
    });
});

module.exports = router;