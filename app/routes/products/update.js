/**
 * Created by Robert on 12/7/16.
 */
"use strict";

var express = require("express"),
    router = express.Router(),
    log = __helper("log"),
    db = __helper("database"),
    validate = __helper("validate"),
    file = "products/update.js",
    errors, id, updates, data, company;

router.route("/:id").put(function (req, res) {
    log.info("/products/update.js: Update product by id");

    var productnumber = req.body.product.productnumber;
    var product = req.body.product;

    db.query("UPDATE product SET btw = ?, name = ?, description = ?, longDescription = ?, price = ? WHERE productNumber = ?",
        [
            product.vat,
            product.name,
            product.description,
            product.longdescription,
            product.price,
            productnumber
        ],
        function( err, result){
            if (err){
                log.error(file + " MySQL error");
                log.error(err);

                return res.status(409).json({ success: false, error: err });
            }

            log.info("product updated");
            return res.status(201).json({ success: true });
    });

});

module.exports = router;