/**
 * Created by Robert on 12/7/16.
 */

"use strict";

var express = require("express"),
    router = express.Router(),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    db =__helper("database"),
    log = __helper("log"),
    file = "/products/create.js: ";

router.route("/").post(function (req, res) {

    log.info("/products/create.js: Create product");

    var product = req.body.product;
    db.query("INSERT INTO product (name, price, btw, description, longdescription) VALUES (?,?,?,?,?)",
        [
            product.name,
            product.price,
            product.vat,
            product.description,
            product.longdescription
        ],
        function(err, result) {
            if (err) {
                log.error(file + "MySQL error");
                log.error(err);

                return res.status(409).json({ success: false, error: err });
            }

            return res.status(201).json({ success: true });
        });
});

module.exports = router;