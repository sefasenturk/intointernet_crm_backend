/**
 * Created by Robert on 12/7/16.
 */

"use strict";

var express = require("express"),
    router = express.Router(),
    cfg = __root("config"),
    crypto = __helper("crypto"),
    db = __helper("database"),
    log = __helper("log");

router.route("/:id").delete(function (req, res) {
    log.info("/products/delete.js: delete product");

    db.query(" DELETE FROM product WHERE productNumber = ?", req.param('id'),function(err, rows) {
        if (err) {
            console.log(err);
            return res.status(304).json(err);
        }

        return res.status(200).json(rows);
    });

});

module.exports = router;